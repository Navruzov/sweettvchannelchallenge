# This module holds fuzzy-classified functionality like 
- pre-processing routines
- validation
- experiment's result tracking
- etc.

---
Back to the [`top`](./../../README.md)