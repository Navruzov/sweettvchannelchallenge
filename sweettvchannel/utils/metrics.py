from typing import List, Union

import numpy as np

APK_DTYPE = Union[list, np.ndarray]
MAPK_DTYPE = Union[List[list], List[np.ndarray], np.ndarray]


def apk(actual: APK_DTYPE,
        predicted: APK_DTYPE, k: int = 5) -> np.float:
    """
    Computes the average precision at k.
    This function computes the average prescision at k
    between two lists of items.

    Parameters
    ----------
    actual : list or np.array
        A list of elements that are to be predicted (order doesn't matter)

    predicted : list or np.array
        A list of predicted elements (order does matter)

    k : int, optional
        The maximum number of predicted elements

    Returns
    -------
    score : float
        The average precision at k over the input lists
    """
    if len(predicted) > k:
        predicted = predicted[:k]

    score = 0.0
    num_hits = 0.0

    for i, p in enumerate(predicted):
        if p in actual and p not in predicted[:i]:
            num_hits += 1.0
            score += num_hits / (i + 1.0)

    if not actual:
        return 0.0

    return score / min(len(actual), k)


def mapk(actual: MAPK_DTYPE, predicted: MAPK_DTYPE, k: int = 5):
    """
    Computes the mean average precision at k.
    This function computes the mean average prescision
    at k between two lists of lists of items.

    Parameters
    ----------
    actual : list or list of lists or list of np.array
             A list of lists of elements that are to be predicted
             (order doesn't matter in the lists)

    predicted : list or list of lists or list of np.array
                A list of lists of predicted elements
                (order matters in the lists)

    k : int, optional
        The maximum number of predicted elements

    Returns
    -------
    score : float
        The mean average precision at k over the input lists
    """
    return np.mean([apk(a, p, k) for a, p in zip(actual, predicted)])
