import gc
import logging
import pickle
import sqlite3
import sys
from collections import defaultdict
from datetime import datetime
from os.path import join as pjoin
from typing import Dict, NoReturn, Tuple, Union
from typing import List

import numpy as np
import pandas as pd
from joblib import Parallel, delayed
from tqdm.auto import tqdm
from os import cpu_count

from pyspark.context import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.session import SparkSession
from os import listdir

logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                    level=logging.INFO, stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

INPUT_DATA_DIR = pjoin('..', 'data', 'raw')
OUTPUT_DATA_DIR = pjoin('..', 'data', 'processed')
UNK_ID = 0
UNK_VAL = '<unk>'
NONAME_VAL = '<noname>'
N_JOBS = max(1, cpu_count() // 2)
PARQUET_ENGINE = 'pyarrow'  # ' fastparquet' or 'pyarrow'
PARQUET_COMPRESSION = 'snappy'


def process_data(input_data_dir: str = INPUT_DATA_DIR,
                 output_data_dir: str = OUTPUT_DATA_DIR,
                 return_data: bool = False,
                 to_parquet: bool = False,
                 keep_initial_codes: bool = True,
                 ) -> Union[NoReturn, Tuple[Union[pd.DataFrame, dict]]]:
    """
    Given initial data sources, apply preprocessing,
    compute categorical encodings
    and store it in optimized format

    Parameters
    ----------
    input_data_dir: str
        The path to raw data

    output_data_dir: str
        The path to store processed data

    return_data: bool, default False
        Whether to save or to save
        and return obtained dfs

    to_parquet: bool, default False
        Whether to convert main dataframes to parquet
        instead of pickling them

    keep_initial_codes: bool, default True
        Whether to keep initial codes or use
        cat_dict[col_name] mapping of reduced ints

    Returns
    -------
    result: None or Tuple
        If `return_data` is True, returns a tuple of
        (views, movies, submission, cat_dict).
        Returns `None` otherwise
    """

    # submission file ######################################################
    logger.info(f'Working with submission file...')
    submission = pd.read_csv(pjoin(input_data_dir, 'submission.csv'))
    logger.info(f'Submission file shape: {submission.shape}')

    # views file ############################################################
    logger.info('Working with `views` file...')

    fname = 'dataset11-30.csv'
    views_db = pd.read_csv(
        pjoin(input_data_dir, fname),
        parse_dates=['start_time', 'stop_time'],
        infer_datetime_format=True,
        dtype={
            'user_id': np.int64,
            'vsetv_id': np.int32,
            'duraton': np.float32,
        }
    ).rename(columns={'duraton': 'duration', 'vsetv_id': 'channel_id'})
    logger.info(f'Initial views shape: {views_db.shape}')
    logger.info(f'`{fname}` size, Mb: {views_db.memory_usage(deep=True).sum() // 2 ** 20}')

    all_ids_in_train = submission.user_id.isin(views_db.user_id.unique()).sum() == len(submission)
    logger.info(f'all submission users in train: {all_ids_in_train}')

    # encode `user_id`
    cat_dict = defaultdict(dict)
    cat_dict['user_id_to_id'] = {
        uid: i + 1 for (i, uid)
        in enumerate(sorted(set(views_db.user_id) | set(submission.user_id)))
    }
    cat_dict['id_to_user_id'] = {v: k for (k, v) in cat_dict['user_id_to_id'].items()}
    # map to reduced ints
    views_db['user_id'] = views_db['user_id'].map(cat_dict['user_id_to_id'])
    submission['user_id'] = submission['user_id'].map(cat_dict['user_id_to_id'])

    # supplementary files ############################################################
    logger.info('Reading tv_program schedules...')
    sup_names = [
        pjoin(input_data_dir, f) for f in [
            'export_arh_11-20-final.csv',
            'export_arh_21-30-final.csv',
            'export_arh_31-42-final.csv'
        ]
    ]

    date_format = "%d.%m.%Y %H:%M:%S"
    drop_zero_tv_show_id = True
    df_sup = pd.DataFrame()

    for sn in sup_names:
        logger.info(f'reading `{sn}` file...')
        sup = pd.read_csv(
            sn,
            dtype={
                'channel_id': np.int32,
                'duration': np.int32,
            },
            low_memory=False,
        )

        if drop_zero_tv_show_id:
            sup = sup[sup.tv_show_id != 0]

        sup['start_time'] = pd.to_datetime(sup['start_time'], format=date_format)
        sup['end_time'] = sup['start_time'] + pd.to_timedelta(sup.duration, unit='s')
        logger.info(f'{sn} ({sup.shape}) size, Mb: {sup.memory_usage(deep=True).sum() // 2 ** 20}')
        df_sup = pd.concat([df_sup, sup], ignore_index=True, sort=False)
        del sup
        gc.collect()

    logger.info(f'Final size ({df_sup.shape}), Mb: {df_sup.memory_usage(deep=True).sum() // 2 ** 20}')

    # correct year of production
    def define_end_year(x: list):
        if len(x) == 1:
            return float(x[0])
        elif len(x) > 1:
            if x[1] and ~np.isnan(float(x[1])):
                return float(x[1])
            elif not x[1] or np.isnan(float(x[1])):
                return float(datetime.now().year)

    _ = df_sup.year_of_production.astype(str).str.replace(
        '(^[0]+-|\s+\?\?\?$|0000$)', 'nan').str.split('-')

    df_sup['show_start_year'] = _.apply(lambda x: float(x[0]))
    df_sup['show_end_year'] = _.apply(define_end_year)

    assert np.mean(df_sup['show_end_year'].fillna(0) >= df_sup['show_start_year'].fillna(0)) == 1.0

    # define channels -----------------
    channels = df_sup.groupby('channel_id').channel_title.apply(lambda x: list(frozenset(x))[0]).reset_index()
    df_sup.drop('channel_title', axis=1, inplace=True)
    logger.info(f'extracting distinct channels: {len(channels)} found')

    # filter views db by channel id that is available in channels info
    logger.info(f'views shape before unknown `channel_id` drop: {views_db.shape}')
    views_db = views_db[views_db.channel_id.isin(df_sup.channel_id.unique())]
    logger.info(f'views shape after drop: {views_db.shape}')

    # define tv_shows ------------------
    shows = df_sup.groupby('tv_show_id')['tv_show_title'].nunique().sort_values().reset_index().rename(
        columns={'tv_show_title': 'episode_cnt'}
    ).set_index('tv_show_id')
    logger.info(f'extracting tv shows (except id == 0): {shows.shape}')

    # substitute indices by ints
    cat_dict['tv_show_id_to_id'] = {k: i + 1 for i, k in enumerate(sorted(shows.index.values))}
    cat_dict['id_to_tv_show_id'] = {v: k for (k, v) in cat_dict['tv_show_id_to_id'].items()}
    shows.index = shows.index.map(cat_dict['tv_show_id_to_id'])
    df_sup['tv_show_id'] = df_sup['tv_show_id'].map(cat_dict['tv_show_id_to_id'])

    # define common substring among names ~ tv show name
    def long_substring(data):
        def substrings(x):
            return {x[i:i + j] for i in range(len(x)) for j in range(len(x) - i + 1)}

        s = substrings(data[0])
        for val in data[1:]:
            s.intersection_update(substrings(val))
        return max(s, key=len)

    shows['unique_tv_names'] = df_sup.groupby('tv_show_id')['tv_show_title'].unique()
    shows['tv_common_name'] = shows['unique_tv_names'].apply(
        lambda x: long_substring([i.strip().lower() for i in x])
    )
    # correct a little where LCS is too short
    idx = (shows.tv_common_name.apply(len) <= 7) & (shows.episode_cnt > 1)
    shows.loc[idx, 'tv_common_name'] = shows.loc[idx, 'unique_tv_names'].apply(lambda x: sorted(x)[0])
    shows['show_start_year'] = df_sup.groupby('tv_show_id')['show_start_year'].min()
    shows['show_end_year'] = df_sup.groupby('tv_show_id')['show_end_year'].max()

    actors = df_sup.actors.fillna('').str.lower().str.split(',')
    actors.index = df_sup['tv_show_id']
    shows['actors'] = actors.reset_index().groupby('tv_show_id')['actors'].apply(list) \
        .apply(lambda l: sorted(set([i for sb in l for i in sb])))

    directors = df_sup.director.fillna('').str.lower().str.split(',')
    directors.index = df_sup['tv_show_id']
    shows['directors'] = directors.reset_index().groupby('tv_show_id')['director'].apply(list) \
        .apply(lambda l: sorted(set([i for sb in l for i in sb])))
    shows['category'] = df_sup.groupby('tv_show_id')['tv_show_category'].unique().apply(lambda x: x[0].lower())

    genres = (
            df_sup.tv_show_genre_1.fillna('').astype(str)
            + ',' + df_sup.tv_show_genre_2.fillna('').astype(str)
            + ',' + df_sup.tv_show_genre_3.fillna('').astype(str)
    ).str.strip().str.split(',').apply(lambda x: [i for i in x if i])
    genres.index = df_sup['tv_show_id']

    shows['genres'] = genres.groupby('tv_show_id').apply(list) \
        .apply(lambda l: sorted(set([i for sb in l for i in sb])))

    # drop redundant columns from `df_sup`
    logger.info('Drop normalized columns from `tv_program` df...')
    logger.info(df_sup.shape)
    df_sup.drop(columns=[
        'tv_show_title', 'tv_show_category', 'tv_show_genre_1', 'tv_show_genre_2',
        'tv_show_genre_3', 'year_of_production', 'director', 'actors',
        'channel_title',
    ], inplace=True, errors='ignore')
    logger.info(df_sup.shape)

    # save preprocessed files ################################################
    logger.info('Saving preprocessed files...')
    if to_parquet:
        df_sup.to_parquet(pjoin(output_data_dir, 'tv_program.parquet'), engine=PARQUET_ENGINE,
                          compression=PARQUET_COMPRESSION)
        views_db.to_parquet(pjoin(output_data_dir, 'views.parquet'), engine=PARQUET_ENGINE,
                            compression=PARQUET_COMPRESSION)
    else:
        df_sup.to_pickle(pjoin(output_data_dir, 'tv_program.pkl'))
        views_db.to_pickle(pjoin(output_data_dir, 'views.pkl'))
    channels.to_pickle(pjoin(output_data_dir, 'channels.pkl'))
    shows.to_pickle(pjoin(output_data_dir, 'shows.pkl'))
    submission.to_pickle(pjoin(output_data_dir, 'submission.pkl'))
    # save cat dict
    with open(pjoin(output_data_dir, 'cat_dict.pkl'), 'wb') as fp:
        pickle.dump(cat_dict, fp)

    if return_data:
        return load_processed_data(processed_data_dir=output_data_dir,
                                   from_parquet=to_parquet,
                                   keep_initial_codes=keep_initial_codes)


def encode_categorical(df: pd.DataFrame, cat_dict: Dict[str, dict],
                       ignore_columns: List[str] = None) -> pd.DataFrame:
    """
    Given category dict and dataframe, encodes columns
    to reduced int codes

    Parameters
    ----------
    df: pd.DataFrame
        Df to reverse encode categoricals

    cat_dict: dict
        Dictionary with computed categorical mappings

    ignore_columns: list, default None
        Whether to ignore some columns
        even if they are found in `cat_dict`

    Returns
    -------
    result: pd.DataFrame
        Reverse encoded dataframe (a copy)
    """
    ignore_columns = [] if not ignore_columns else ignore_columns
    df_cp = df.copy(deep=True)
    idx_name = df_cp.index.name
    df_cp = df_cp.reset_index(drop=False if idx_name else True)
    for c in df_cp.columns:
        if f'{c}_to_id' in cat_dict and c not in ignore_columns:
            try:
                df_cp[c] = df_cp[c].map(cat_dict[f'{c}_to_id'])
            except (TypeError, ValueError):
                df_cp[c] = df_cp[c].apply(lambda x: [cat_dict[f'{c}_to_id'][i] for i in x] if x else [UNK_ID])
    return df_cp.set_index(idx_name) if idx_name else df_cp


def inverse_encode_categorical(df: pd.DataFrame, cat_dict: Dict[str, dict],
                               ignore_columns: List[str] = None) -> pd.DataFrame:
    """
    Given category dict and dataframe, encodes columns
    back to initial format (if any)

    Parameters
    ----------
    df: pd.DataFrame
        Df to reverse encode categoricals

    cat_dict: dict
        Dictionary with computed categorical mappings

    ignore_columns: list, default None
        Whether to ignore some columns
        even if they are found in `cat_dict`

    Returns
    -------
    result: pd.DataFrame
        Reverse encoded dataframe
    """
    ignore_columns = [] if not ignore_columns else ignore_columns
    df_cp = df.copy(deep=True)
    idx_name = df_cp.index.name
    df_cp = df_cp.reset_index(drop=False if idx_name else True)
    for c in df_cp.columns:
        if f'{c}_to_id' in cat_dict and c not in ignore_columns:
            try:
                df_cp[c] = df_cp[c].map(cat_dict[f'id_to_{c}'])
            except (TypeError, ValueError):
                df_cp[c] = df_cp[c].apply(lambda x: [cat_dict[f'id_to_{c}'][i] for i in x] if x else [UNK_ID])
    return df_cp.set_index(idx_name) if idx_name else df_cp


def load_processed_data(processed_data_dir: str = OUTPUT_DATA_DIR,
                        from_parquet: bool = False, keep_initial_codes: bool = True) -> Tuple:
    """
    Load preprocessed files back

    Parameters
    ----------
    processed_data_dir: str
        The path to processed files

    from_parquet: bool, default False
        Whether to load main dataframes (views, tv_channel)
        from .parquet or from pickled data

    Returns
    -------
    results: tuple
        A tuple of (views, tv_program, channels, shows, submission, cat_dict)
        of type (pd.DataFrame, ..., pd.DataFrame, dict)

    """
    with open(pjoin(processed_data_dir, 'cat_dict.pkl'), 'rb') as fp:
        cat_dict = pickle.load(fp)

    if from_parquet:
        views = pd.read_parquet(pjoin(processed_data_dir, 'views.parquet'),
                                engine=PARQUET_ENGINE,
                                # compression=PARQUET_COMPRESSION
                                )
        tv_program = pd.read_parquet(pjoin(processed_data_dir, 'tv_program.parquet'),
                                     engine=PARQUET_ENGINE,
                                     # compression=PARQUET_COMPRESSION
                                     )
    else:
        views = pd.read_pickle(pjoin(processed_data_dir, 'views.pkl'))
        tv_program = pd.read_pickle(pjoin(processed_data_dir, 'tv_program.pkl'))

    submission = pd.read_pickle(pjoin(processed_data_dir, 'submission.pkl'))
    channels = pd.read_pickle(pjoin(processed_data_dir, 'channels.pkl'))
    shows = pd.read_pickle(pjoin(processed_data_dir, 'shows.pkl'))

    if keep_initial_codes:
        views = inverse_encode_categorical(views, cat_dict=cat_dict)
        tv_program = inverse_encode_categorical(tv_program, cat_dict=cat_dict)
        channels = inverse_encode_categorical(channels, cat_dict=cat_dict)
        shows = inverse_encode_categorical(shows, cat_dict=cat_dict)
        submission = inverse_encode_categorical(submission, cat_dict=cat_dict)

    return views, tv_program, channels, shows, submission, cat_dict


def construct_train_skeleton_spark(
        processed_data_dir: str = OUTPUT_DATA_DIR
):
    """
    Given list of `channel_id`, constructs chunked
    view and tv_schedule intersections in in-memory DB
    to obtain (user_id, tv_show_id) interactions

    Parameters
    ----------
    processed_data_dir: str
        Path where processed `views.parquet`
        and `tv_program.parquet` are located

    Returns
    -------
    df_train: pd.DataFrame
        Training skeleton of (user_id, tv_show_id) interactions
    """
    sc = SparkContext('local')
    with sc:
        spark = SparkSession(sc)
        sqlContext = SQLContext(sc)

        views_spark = spark.read.parquet(pjoin(processed_data_dir, "views.parquet"))
        tv_spark = spark.read.parquet(pjoin(processed_data_dir, "tv_program.parquet"))
        views_spark.registerTempTable("views")
        tv_spark.registerTempTable("tv")

        # define merge query
        spark_query = """
        select 
            views.user_id as user_id,
            views.channel_id as channel_id,
            views.start_time as session_start_time,
            views.stop_time as session_end_time,
            views.duration as session_duration,
            tv.tv_show_id as tv_show_id,
            tv.start_time as tv_show_start_time,
            tv.duration as tv_show_duration,
            tv.end_time as tv_show_end_time
        from 
            views inner join tv on views.channel_id = tv.channel_id 
            and (views.stop_time >= tv.start_time and views.start_time <= tv.end_time)
        """
        # get results and store it locally
        results = sqlContext.sql(spark_query)
        spark_dir = pjoin(processed_data_dir, 'df_train.parquet')
        results.coalesce(1).write.mode('overwrite').parquet(spark_dir)
        # stop context
        sc.stop()
        fname = [f for f in listdir(spark_dir) if f[-8:] == '.parquet'][0]
        # read it
        df_train = pd.read_parquet(pjoin(spark_dir, fname))
        print('Calculating duration features...')
        df_train['view_start_time'] = df_train[['session_start_time', 'tv_show_start_time']].max(axis=1)
        df_train['view_end_time'] = df_train[['session_end_time', 'tv_show_end_time']].min(axis=1)
        df_train['view_duration'] = (
                df_train['view_end_time'] - df_train['view_start_time']
        ).dt.total_seconds().astype(np.float32)

        df_train['view_percentage'] = (
                df_train['view_duration'] / df_train['tv_show_duration']).astype(np.float32)

        print(f'Obtained skeleton shape: {df_train.shape}')
        return df_train.drop_duplicates()
